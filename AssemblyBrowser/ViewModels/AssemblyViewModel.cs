﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using AssemblyBrowser.Commands;
using InfoCollector.Models.CustomAssembly;
using static InfoCollector.Models.InfoCollector;

namespace AssemblyBrowser.ViewModels
{
    public class AssemblyViewModel : INotifyPropertyChanged
    {
        //public List<AssemblyInfo> AssemblyInfos = null;

        private ObservableCollection<AssemblyInfo> assemblyInfos = new ObservableCollection<AssemblyInfo>();

        public AssemblyViewModel()
        {
            loadCommand = new LoadAssemblyCommand(RenewList);
        }

        public ObservableCollection<AssemblyInfo> AssemblyInfos
        {
            get => assemblyInfos;
            set
            {
                assemblyInfos = value;
                OnAssemblyInfosChange(nameof(assemblyInfos));
            }
        }

        public LoadAssemblyCommand loadCommand { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;

        //[NotifyPropertyChangedInvocator]
        //private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        //{
        //    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        //}

        protected virtual void OnAssemblyInfosChange(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void RenewList(string path)
        {
            AssemblyInfos.Add(LoadAssembly(path));
            //assemblyInfos = new ObservableCollection<AssemblyInfo>(new DllTreeProvider(path).getTree());
        }

        //public event PropertyChangedEventHandler PropertyChanged;


        //private ICommand loadAsmCommand = null;
        //public ICommand LoadAsmCommand {
        //    get
        //    {
        //        return loadAsmCommand;
        //    }
        //}

        //public void OnLoadCommand()
        //{
        //    OpenFileDialog openFileDialog = new OpenFileDialog
        //    {
        //        InitialDirectory = Assembly.GetEntryAssembly().Location,
        //        Filter = "dll files (*.dll)|*.dll",
        //        RestoreDirectory = true
        //    };


        //    if (openFileDialog.ShowDialog() == true)
        //    {
        //        AssemblyInfos.Add(InfoCollector.LoadAssembly(openFileDialog.FileName));
        //    }
        //}

        //public AssemblyViewModel()
        //{
        //    AssemblyInfos = new ObservableCollection<AssemblyInfo>();
        //    AssemblyInfos.Add(InfoCollector.LoadAssembly("D:/bsuir/5sem/spp/AssemblyBrowser/ShapesLib/bin/Debug/ShapesLib.dll"));
        //    loadAsmCommand = new LoadAssemblyCommand(new Action(OnLoadCommand));
        //}

        ////public void UpdateAssemblyList(string assemblyPath)
        ////{
        ////    AssemblyInfos.Add(InfoCollector.LoadAssembly(assemblyPath));

        ////    OnAssemblyInfosChange(nameof(AssemblyInfos));
        ////}

        //protected virtual void OnAssemblyInfosChange(string propertyName)
        //{
        //    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        //}
    }
}