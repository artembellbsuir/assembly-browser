﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace InfoCollector.Models.CustomAssembly
{
    public class AssemblyInfo
    {
        public string Name { get; set; }
        public List<NamespaceInfo> Namespaces { get; set; }
        
        public AssemblyInfo(Assembly assembly)
        {
            Name = assembly.FullName;
            var asmNamespaces = new Dictionary<string, NamespaceInfo>();
            foreach (var asmType in assembly.GetTypes())
            {
                var asmNamespace = asmType.Namespace;
                if (!asmNamespaces.ContainsKey(asmNamespace))
                {
                    asmNamespaces[asmNamespace] = new NamespaceInfo(asmType.Namespace);
                }
                asmNamespaces[asmNamespace].AddType(asmType);
            }
            Namespaces = asmNamespaces.Values.ToList();
        }
    }
}