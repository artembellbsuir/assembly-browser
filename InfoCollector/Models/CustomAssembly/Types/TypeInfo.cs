﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace InfoCollector.Models.CustomAssembly.Types
{
    public class TypeInfo : INotifyPropertyChanged
    {
        public List<AsmTypeFieldInfo> FieldInfos { get; set; }
        public List<AsmTypeMethodInfo> MethodInfos { get; set; }
        public List<AsmTypePropertyInfo> PropertyInfos { get; set; }
        public string Name => Type.ToString();
        public event PropertyChangedEventHandler PropertyChanged;

        public Type Type;

        public TypeInfo(Type type)
        {
            Type = type;

            FieldInfos = new List<AsmTypeFieldInfo>();
            PropertyInfos = new List<AsmTypePropertyInfo>();
            MethodInfos = new List<AsmTypeMethodInfo>();

            foreach (var fieldInfo in type.GetFields())
            {
                FieldInfos.Add(new AsmTypeFieldInfo(fieldInfo));
            }

            foreach (var propertyInfo in type.GetProperties())
            {
                PropertyInfos.Add(new AsmTypePropertyInfo(propertyInfo));
            }

            foreach (var propertyInfo in type.GetMethods())
            {
                MethodInfos.Add(new AsmTypeMethodInfo(propertyInfo));
            }
        }
    }
}