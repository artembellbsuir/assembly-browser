﻿using System;
using System.Reflection;

namespace InfoCollector.Models.CustomAssembly.Types
{
    public class AsmTypeFieldInfo
    {
        public string name;
        public Type type;

        public AsmTypeFieldInfo(FieldInfo fieldInfo)
        {
            type = fieldInfo.FieldType;
            name = fieldInfo.Name;
        }

        public string Signature => ToString();

        public override string ToString()
        {
            return string.Format("{0}: {1}", name, type);
        }
    }
}