﻿using System;
using System.Reflection;

namespace InfoCollector.Models.CustomAssembly.Types
{
    public class AsmTypePropertyInfo
    {
        public string name;
        public Type type;

        public AsmTypePropertyInfo(PropertyInfo propertyInfo)
        {
            type = propertyInfo.PropertyType;
            name = propertyInfo.Name;
        }

        public override string ToString()
        {
            return string.Format("{0}: {1}", name, type);
        }
    }
}