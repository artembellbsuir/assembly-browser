﻿using System.Reflection;

namespace InfoCollector.Models.CustomAssembly
{
    public class AssemblyLoader
    {
        private static string _baseAssemblyPath = null;

        public AssemblyLoader(string baseAssemblyPath = "")
        {
        }

        public static Assembly LoadAssembly(string relativeAssemblyPath)
        {
            var _baseAssemblyPath = "";
            var assemblyRelativePath = _baseAssemblyPath == ""
                ? relativeAssemblyPath
                : _baseAssemblyPath + "/" + relativeAssemblyPath;
            var asm = Assembly.LoadFrom(assemblyRelativePath);
            return asm;
        }
    }
}