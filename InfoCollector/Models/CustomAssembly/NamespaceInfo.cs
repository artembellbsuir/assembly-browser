﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using InfoCollector.Models.CustomAssembly.Types;

namespace InfoCollector.Models.CustomAssembly
{
    public class NamespaceInfo
    {
        public string Name { get; set; }
        public List<TypeInfo> Types { get; set; }

        public NamespaceInfo(string asmNamespace)
        {
            Name = asmNamespace;
            Types = new List<TypeInfo>();
        }

        public void AddType(Type type)
        {
            Types.Add(new TypeInfo(type));
        }
    }
}